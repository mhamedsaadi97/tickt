/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      colors:{
        customYellow : '#F7C107',
        customGrey : '#D9D9D9',
        lightGrey : '#F4F5FC',
      },
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
          "left-icon" : "url('/edit.png')",
          
      },
      backgroundPosition:{
        "left-plus-five" : "5px center",
      },
      width:{
        'max-content': "max-content",
      },
      margin:{
        'auto': "auto",
      },
    },
  },
  plugins: [],
};
