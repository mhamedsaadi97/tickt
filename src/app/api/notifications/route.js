import { promises as fs } from 'fs';
import path from 'path';
import { NextResponse } from 'next/server';

const filePath = path.join(process.cwd(), 'mockData', 'notifications.json');

export async function GET() {
  try {
    const fileContents = await fs.readFile(filePath, 'utf8');
    const notifications = JSON.parse(fileContents);
    return NextResponse.json(notifications);
  } catch (error) {
    return NextResponse.json({ message: 'Error reading notifications file', error }, { status: 500 });
  }
}

export async function POST(req) {
  try {
    const newNotification = await req.json();
    console.log('Received new notification:', newNotification); // Log the received notification

    const fileContents = await fs.readFile(filePath, 'utf8');
    const notifications = JSON.parse(fileContents);

    // Generate a new ID for the notification
    const newId = notifications.length ? notifications[notifications.length - 1].id + 1 : 1;
    newNotification.id = newId;

    notifications.push(newNotification);
    await fs.writeFile(filePath, JSON.stringify(notifications, null, 2));

    return NextResponse.json(newNotification, { status: 201 });
  } catch (error) {
    return NextResponse.json({ message: 'Error writing to notifications file', error }, { status: 500 });
  }
}
