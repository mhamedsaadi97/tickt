export async function addNotification(notification) {
    const response = await fetch('/api/notifications', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(notification),
    });
  
    if (!response.ok) {
      throw new Error('Failed to add notification');
    }
  
    return await response.json();
  }
  