// app/api/events/getEvents.js
export async function getFirstEvent() {
    const response = await fetch('/api/events');
    if (!response.ok) {
      throw new Error('Failed to fetch events');
    }
    const events = await response.json();
    const firstEvent = events[0];
  
    // Parse the date field
    const eventDate = new Date(firstEvent.date);
  
    // Parse startTime and endTime strings and set the hours and minutes on eventDate
    const startDateParts = firstEvent.startTime.split(':');
    const startDate = new Date(eventDate);
    startDate.setHours(parseInt(startDateParts[0], 10));
    startDate.setMinutes(parseInt(startDateParts[1], 10));
  
    const endDateParts = firstEvent.endTime.split(':');
    const endDate = new Date(eventDate);
    endDate.setHours(parseInt(endDateParts[0], 10));
    endDate.setMinutes(parseInt(endDateParts[1], 10));
  
    // Format date, day, month, start time, and end time
    const options = {
      weekday: 'long',
      month: 'long',
      day: 'numeric',
      hour: 'numeric',
      minute: '2-digit'
    };
  
    const formattedStartDate = startDate.toLocaleDateString('en-US', options);
    const formattedEndDate = endDate.toLocaleTimeString('en-US', { hour: 'numeric', minute: '2-digit' });
  
    // Calculate time difference
    const currentTime = new Date();
    const timeDifference = startDate - currentTime;
  
    // Convert time difference to days, hours, and minutes
    const days = Math.floor(timeDifference / (1000 * 60 * 60 * 24));
    const hours = Math.floor((timeDifference % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    const minutes = Math.floor((timeDifference % (1000 * 60 * 60)) / (1000 * 60));
  
    // Format time left message
    let timeLeftMessage = '';
    if (days > 0) {
      timeLeftMessage += `${days} day${days > 1 ? 's' : ''} `;
    }
    if (hours > 0 || days > 0) {
      timeLeftMessage += `${hours}:${minutes < 10 ? '0' : ''}${minutes} hours left`;
    } else {
      timeLeftMessage += `${minutes} minutes left`;
    }
  
    return {
      ...firstEvent,
      formattedStartDate,
      formattedEndDate,
      timeLeftMessage
    };
  }
  