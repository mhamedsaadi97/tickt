import Header from "@/components/Header";
import TextEditor from "@/components/TextEditor";
import Notifications from "@/components/Notifications";


export default function Home() {
    
  return (
    <div className="mainContainer py-5 pl-16 pr-7 bg-lightGrey">
    <Header />
    <div className="flex">

        <div className="w-2/3">
        <TextEditor />
        </div>
        <div className="w-1/3 ml-5">
          <h2 className="font-semibold text-xl">Meeting Stream</h2>
          <Notifications />
        </div>
        
        </div>

    </div>
       
  );
};


