
import "./globals.css";

export const metadata = {
  title: "Tickt",
  description: "Assesment",
};

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body className="bg-lightGrey">{children}</body>
    </html>
  );
}
