import React from 'react'

const TextEditorHeader = () => {
  return (
    <div className='textEditorHeader flex gap-6 my-5'>
    <a  className="w-max-content inline-block mt-auto mb-auto" href="#"> <img  src="/stack.png" /></a>
    <a className='noteItem bg-white bg-left-icon bg-no-repeat w-max-content inline-block
     bg-left-plus-five text-xs font-normal text-center px-7 py-3 rounded' href="#">
        
       Todays's notes
        
    </a>
    <a className='noteItem bg-white  bg-no-repeat w-max-content inline-block
     bg-left-plus-five text-xs font-normal text-center px-7 py-3 rounded' href="#">
        
        Yesterday’s notes
        
    </a>
 </div>
  )
}

export default TextEditorHeader