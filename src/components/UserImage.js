import React, { useState } from 'react';
import PropTypes from 'prop-types';

const UserImage = ({ imageSrc, size=41, alt='u'}) => {

    const [isImageValid, setIsImageValid] = useState(true);
    const handleImageError = () => {
        setIsImageValid(false);
      };
      
    const sizeClasses = {
        width: size,
        height: size,
      };

    return (
        <div
          className="flex items-center justify-center rounded-full bg-gray-300 mx-1"
          style={sizeClasses}
        >
          {isImageValid ? (
            <img
              src={imageSrc}
              alt={alt}
              className="w-full h-full object-cover rounded-full"
              onError={handleImageError}
            />
          ) : (
            <span className="text-white" style={{ fontSize: size * 0.4 }}>
              {alt.charAt(0).toUpperCase()}
            </span>
          )}
        </div>
      );
    };
    
    UserImage.propTypes = {
      imageSrc: PropTypes.string.isRequired,
      size: PropTypes.number,
      alt: PropTypes.string,
    };
    
   

export default UserImage