import React from 'react'
import UserImage from './UserImage'

const Notification = ({ notification }) => {
  return (

    <div className="notification flex my-3">
    <UserImage imageSrc="/png.png" size={41} alt='J'  key="j" />
    
    <div className="user-name">{notification.userId.name}</div>
    <div className="message">
       
        <div> {notification.message}</div>
        {notification.description && (
      <div className="description p-3 bg-white mt-3" 
      dangerouslySetInnerHTML={{ __html: notification.description }}></div>
    )}
        </div> 
    
  </div>

  )
}

export default Notification