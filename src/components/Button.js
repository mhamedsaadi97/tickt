import React from 'react'
import PropTypes from 'prop-types';

const Button = ({ onClick, imageSrc=null, text='', children=null, className='' }) => {
  return (
    <button className={`btn bg-customGrey rounded-3xl py-3 px-7 ${className}`} onClick={onClick}>
      {imageSrc && <img src={imageSrc} alt="Button Icon" className="btn-image" />}
      {text && <span className="btn-text">{text}</span>}
      {children}
    </button>
  )
};

Button.propTypes = {
    onClick: PropTypes.func.isRequired,
    imageSrc: PropTypes.string,
    text: PropTypes.string,
    children: PropTypes.node,
    className: PropTypes.string,
  };
  
  

export default Button