import React from 'react'
import PropTypes from 'prop-types';

const ProgressBar = ({ progress }) => {
  return (
    <div className="w-full bg-customGrey rounded-full h-1.5">
    <div
      className="bg-customYellow h-1.5 rounded-full"
      style={{ width: `${progress}%` }}
    ></div>
  </div>
  )
}

ProgressBar.propTypes = {
    progress: PropTypes.number.isRequired,
  };

export default ProgressBar