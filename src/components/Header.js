'use client';
import React, { useEffect, useState } from 'react';
import ProgressBar from './ProgressBar'
import { getFirstEvent } from '@/app/api/events/getEvents';

const Header = () => {
    const [progress, setProgress] = useState(25);
    const [event, setEvent] = useState([]);

    useEffect(() => {
      const fetchData = async () => {
        try {
          const eventsData = await getFirstEvent();
          setEvent(eventsData);
        } catch (error) {
          console.error('Error fetching events:', error);
        }
      };
  
      fetchData();
    }, []);

    
  return (
    <>
    <img  className="my-5" src="/logo.png" alt="Example" />
    <h2 className='text-xl font-semibold'>{event.title}</h2>
    <div className='dayDateTimeContainer grid grid-cols-1 md:grid-cols-4 gap-4'>
        <div className='dayDateTime py-4'>
        
            <span className='text-xs font-normal text-[#222B45] mr-5'>{event.formattedStartDate}  {event.formattedEndDate}</span>
       </div>
        <div className='progressBar py-4 flex items-center'>
        <ProgressBar progress={progress} />
        </div>
        <div className='timer py-4'>
            <span className='text-xs text-customYellow'> {event.timeLeftMessage}</span>    
           
        </div>
    </div>
    </>
  )
}

export default Header