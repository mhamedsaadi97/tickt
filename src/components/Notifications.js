'use client';
import React ,{useState,useEffect} from 'react'
import Notification from './Notification';
import { getNotifications } from '@/app/api/notifications/getNotifications';

const Notifications = () => {

    const [notifications, setNotifications] = useState([]);
    useEffect(() => {
        const fetchData = async () => {
          try {
            const data = await getNotifications();
            setNotifications(data);
          } catch (error) {
            console.error('Error fetching notifications:', error);
          }
        };
    
        fetchData();
      }, []);

  return (
      <div className="notifications-container">
      
      {notifications.map((notification, index) => (
        <Notification key={index} notification={notification} />
      ))}
    </div>
  )
}

export default Notifications