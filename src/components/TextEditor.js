'use client';
import React, { useEffect, useState } from 'react';
import TextEditorHeader from './TextEditorHeader'
import ReactQuill from 'react-quill';
import Button from './Button';
import UserImage from './UserImage';
import { addNotification } from '@/app/api/notifications/addNotification';
import 'react-quill/dist/quill.snow.css';



const TextEditor = ({ currentUserId=1, currentEventId=1 }) => {
    
    const [users, setUsers] = useState([]);
    const [description, setDescription] = useState('');
    const [error, setError] = useState(null);

    

    useEffect(() => {
      fetch('/api/users')
        .then((response) => response.json())
        .then((data) => setUsers(data.slice(0, 3)));
    }, []);

    
    const handleAddNotification = async () => {
      const notification = {
        userId: currentUserId,
        eventId: currentEventId,
        message: `User ${currentUserId} has added a description to event ${currentEventId}:`,
        description,
        createdAt: new Date().toISOString(),
        isRead: false,
      };
  
      try {
        await addNotification(notification);
        alert('Notification added successfully');
      } catch (error) {
        console.error('Error adding notification:', error);
      }
    };
  

  return (
    <div className=''>
     
    <TextEditorHeader  />

    <ReactQuill theme="snow" value={description} onChange={setDescription}
    modules={{
        toolbar: [
          [{ 'header': '1'}, {'header': '2'}, { 'font': [] }],
          [{size: []}],
          ['bold', 'italic', 'underline', 'strike', 'blockquote'],
          [{'list': 'ordered'}, {'list': 'bullet'}, 
           {'indent': '-1'}, {'indent': '+1'}],
          ['link', 'image', 'video'],
          ['clean']                                         
        ],
      }}
      formats={[
        'header', 'font', 'size',
        'bold', 'italic', 'underline', 'strike', 'blockquote',
        'list', 'bullet', 'indent',
        'link', 'image', 'video'
      ]} />

      <div className='flex justify-between mt-5 items-center'>
        <div className='share flex items-center'>
         <p className='mr-5'>Shared With</p>
         {users.map((user) => (   
            <UserImage imageSrc={user.image} size={41} alt={user.name}  key={user.name}/>
        ))}
      
        </div>
        <div>
          <Button onClick={() => handleAddNotification()} imageSrc="/send.png" text="" />
          </div>
      </div>
    
    </div>
  )
}





export default TextEditor